package com.devcamp.s50.account_rest_api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AccountRestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(AccountRestApiApplication.class, args);
	}

}
