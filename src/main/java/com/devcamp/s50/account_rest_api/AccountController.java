package com.devcamp.s50.account_rest_api;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AccountController {
    @CrossOrigin
    @GetMapping("/accounts")
    public ArrayList<Account> getAccountRestApi(){
        Account Duong = new Account(2, "Duong", 2500);
        Account Linh = new Account(3, "Linh", 4000);
        Account Tuan = new Account(4, "Tuan");
        System.out.println("Account Duong:  " + Duong);
        System.out.println("Account Linh:  " + Linh);
        System.out.println("Account Tuan:  " + Tuan);

        Duong.credit(2000);
        Linh.debit(500);
        Linh.transferTo(Duong, 1200);
        System.out.println("===Sau Khi Giao Dich===");
        System.out.println("Account Duong :  " + Duong);
        System.out.println("Account Linh :  " + Linh);

        ArrayList<Account> DanhSachNguoi = new ArrayList<>();
        DanhSachNguoi.add(Duong);
        DanhSachNguoi.add(Linh);
        DanhSachNguoi.add(Tuan);

        return DanhSachNguoi;
    }
}
